//
//  ViewController.swift
//  swiftLA
//
//  Created by Richard Morgan on 12/09/2016.
//  Copyright © 2016 Richard Morgan. All rights reserved.
//

import Foundation
import UIKit


class ViewController: UIViewController {

    @IBOutlet var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let url = NSURL (string: "https://www.cafex.com");
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
        
        
    }
    
    override func viewDidLayoutSubviews() {
        print(webView.frame.size); // This is actual size you are looking for
        self.webView.frame = self.view.bounds
        self.webView.scalesPageToFit = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func assist(sender: UIButton) {
        
        print("Clicked");
        
        let config = ["destination": "agent1", "acceptSelfSignedCerts": true, "isAgentWindowOnTop": true];
        print(config);
        
        AssistSDK.addDelegate(self);
        AssistSDK.startSupport("https://172.31.250.62:8443", supportParameters: config);
        

    }
    

}

